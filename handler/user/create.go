package user

import (
	"github.com/gin-gonic/gin"
	. "server_api/handler"
	"server_api/pkg/errno"
	"github.com/lexkong/log"
	"fmt"
	"net/http"
)

func Create(c *gin.Context){
	var r CreateRequest
	if err := c.Bind(&r); err != nil{
		c.JSON(http.StatusOK, gin.H{"error": errno.ErrBind})
		return
	}
	admin2 := c.Param("username")
	log.Infof("URL username: %s", admin2)
	desc := c.Query("desc")
	log.Infof("URL key param desc: %s", desc)
	contentType := c.GetHeader("Content-Type")
	log.Infof("Header Content-Type: %s", contentType)

	log.Debugf("username is: [%s], password is [%s]", r.Username, r.Password)
	if r.Username == ""{
		SendResponse(c, errno.New(errno.ErrUserNotFound, fmt.Errorf("username can not found in db: xx.xx.xx.xx")), nil)
		return
	}

	if r.Password == ""{
		SendResponse(c, fmt.Errorf("password is empty"), nil)
	}
	rsp := CreateResponse{
		Username: r.Username,
	}
	SendResponse(c, nil, rsp)
}