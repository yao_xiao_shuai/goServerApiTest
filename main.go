package main

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/lexkong/log"
	"net/http"
	"server_api/router"
	"time"
	"github.com/spf13/pflag"
	"server_api/config"
	"github.com/spf13/viper"
	"server_api/model"
)

var (
	cfg = pflag.StringP("config","c","","apiserver config file path")
)

func main() {
	pflag.Parse()
	//init config
	if err := config.Init(*cfg);err != nil{
		panic(err)
	}
	//init db
	model.DB.Init()
	defer model.DB.Close()
	//for {
	//	fmt.Println(viper.GetString("runmode"))
	//	time.Sleep(4*time.Second)
	//}
	//create gin engine
	g := gin.New()
	//set gin mode
	gin.SetMode(viper.GetString("runmode"))
	middlewares := []gin.HandlerFunc{}
	//Routes
	router.Load(
		g, middlewares...,
	)
	//ping the server to make sure the router is working
	go func() {
		if err := pingServer(); err != nil {
			log.Fatal("The router has no response, or it might took too long to start up.", err)
		}
		log.Info("The router has been deployed successfully.")
	}()
	log.Infof("Start to listening the incoming requests on http address: %s", viper.GetString("addr"))
	log.Infof(http.ListenAndServe(viper.GetString("addr"), g).Error())
}

func pingServer() error {
	for i := 0; i < viper.GetInt("max_ping_count"); i++ {
		resp, err := http.Get(viper.GetString("url") + "/sd/health")
		if err == nil && resp.StatusCode == 200 {
			return nil
		}
		//Sleep for a second for next ping
		log.Info("Waiting for the router, retry in 1 second.")
		time.Sleep(time.Second)
	}
	return errors.New("Cannot connect to the router.")
}
